import Login from './theme_pages/Authentication/Login'
import Menu from './theme_components/HorizontalLayout/index'
import Dashboard from './theme_pages/Dashboard/index'
import FormElements from './theme_pages/Forms/FormElements/index'
import FromLayouts from './theme_pages/Forms/FormLayouts'
import UiTabsAccordions from './theme_pages/UI/UiTabsAccordions'
import UiProgressbar from './theme_pages/UI/UiProgressbar'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/pages-login">
          <Login />
        </Route>
        <Route path="/dashboard">
          <Menu />
          <Dashboard />
        </Route>
        <Route path="/form-elements">
          <Menu />
          <FormElements />
        </Route>
        <Route path="/form-layouts">
          <Menu />
          <FromLayouts />
        </Route>
        <Route path="/ui-tabs-accordions">
          <Menu />
          <UiTabsAccordions />
        </Route>
        <Route path="/ui-progressbars">
          <Menu />
          <UiProgressbar />
        </Route>
        <Route path="/">
          <Menu />
          <Dashboard />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
